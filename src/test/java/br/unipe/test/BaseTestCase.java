package br.unipe.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

import br.unipe.common.Property;
import br.unipe.common.Selenium;

public class BaseTestCase {
	
	protected static WebDriver driver;
	protected static Property property;
	
	@BeforeClass
	public static void setUp() {
		Property.loadProperties();
		driver = Selenium.getDriver();
		driver.manage().window().maximize();
	}
	
	@AfterClass
	public static void tearDown() {
		Selenium.resetDriver();
		driver.quit();
	}

}