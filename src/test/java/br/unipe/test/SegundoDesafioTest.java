package br.unipe.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.unipe.common.Parametro;
import br.unipe.common.Selenium;
import br.unipe.page.BasePage;
import br.unipe.page.SegundoDesafioPage;

public class SegundoDesafioTest extends BaseTestCase {

	BasePage basePage = new BasePage();
	SegundoDesafioPage segundoDesafioPage = new SegundoDesafioPage();

	@Before
	public void before() {
		System.out.println("=================================================================");
		Parametro.CONTADOR_SCREENSHOT = "000";
	}

	@After
	public void after() {
		System.out.println("Teste "+Parametro.METHOD_NAME_TEST+" finalizado!");
		System.out.println("=================================================================");
	}

	@Test
	public void segundoDesafio() {
		Parametro.METHOD_NAME_TEST = "Segundo Desafio";
		Selenium.getDriver().get("http://eliasnogueira.com/arquivos_blog/selenium/desafio/2desafio/");
		segundoDesafioPage.segundoDesafio();
	}
	
}