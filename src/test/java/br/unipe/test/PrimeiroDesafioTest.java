package br.unipe.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.unipe.common.Parametro;
import br.unipe.common.Selenium;
import br.unipe.page.BasePage;
import br.unipe.page.PrimeiroDesafioPage;

public class PrimeiroDesafioTest extends BaseTestCase {

	BasePage basePage = new BasePage();
	PrimeiroDesafioPage primeiroDesafioPage = new PrimeiroDesafioPage();

	@Before
	public void before() {
		System.out.println("=================================================================");
		Parametro.CONTADOR_SCREENSHOT = "000";
	}

	@After
	public void after() {
		System.out.println("Teste "+Parametro.METHOD_NAME_TEST+" finalizado!");
		System.out.println("=================================================================");
	}

	@Test
	public void primeiroDesafio() {
		Parametro.METHOD_NAME_TEST = "Primeiro Desafio";
		Selenium.getDriver().get("http://eliasnogueira.com/arquivos_blog/selenium/desafio/1desafio/");
		primeiroDesafioPage.primeiroDesafio();
	}

}