package br.unipe.page;

import org.junit.Assert;
import org.openqa.selenium.By;

import br.unipe.util.Utils;

public class PrimeiroDesafioPage extends BasePage {
	
	private By LABEL_NUMERO1 = By.id("number1");
	private By LABEL_NUMERO2 = By.id("number2");
	private By INPUT_CAIXA_DE_TEXTO = By.name("soma");
	private By BTN_ENVIAR = By.name("submit");
	private By RESULTADO = By.id("resultado");
	
	private int soma = 0;
	private String resultadoEsperado = "CORRETO";
	
	public void primeiroDesafio() {
		realizarSoma();
		preencherCaixaDeTexo();
		clickBotaoEnviar();
		validarResultado();
	}

	private void realizarSoma() {
		Utils.takeScreenshot(false);
		String numero1 = driver.findElement(LABEL_NUMERO1).getText();
		String numero2 = driver.findElement(LABEL_NUMERO2).getText();
		soma = Integer.parseInt(numero1) + Integer.parseInt(numero2);
		System.out.println("Soma: " + numero1 + " + " + numero2 + " = " + Integer.toString(soma));
	}

	private void preencherCaixaDeTexo() {
		driver.findElement(INPUT_CAIXA_DE_TEXTO).sendKeys(Integer.toString(soma));
		Utils.takeScreenshot(false);
	}

	private void clickBotaoEnviar() {
		driver.findElement(BTN_ENVIAR).click();
		System.out.println("Enviando resultado...");
	}

	private void validarResultado() {
		aguardarElementoVisivel(driver.findElement(RESULTADO));
		Utils.takeScreenshot(false);
		Assert.assertEquals(resultadoEsperado, driver.findElement(RESULTADO).getText());
		System.out.println("Resultado validado com sucesso! ["+resultadoEsperado+"]");
	}

}