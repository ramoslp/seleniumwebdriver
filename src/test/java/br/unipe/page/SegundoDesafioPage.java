package br.unipe.page;

import org.junit.Assert;
import org.openqa.selenium.By;

import br.unipe.util.Utils;

public class SegundoDesafioPage extends BasePage {
	
	private By DISPLAY_NOME = By.id("name_rg_display_section");
	private By DISPLAY_EMAIL = By.id("email_rg_display_section");
	private By DISPLAY_TELEFONE = By.id("phone_rg_display_section");
	private By INPUT_NOME = By.id("nome_pessoa");
	private By INPUT_EMAIL = By.id("email_value");
	private By INPUT_TELEFONE = By.id("phone_value");
	private By BTN_SALVAR_NOME = By.xpath("//*[@id='name_hv_editing_section']/input[2]");
	private By BTN_SALVAR_EMAIL = By.xpath("//*[@id='email_hv_editing_section']/input[2]");
	private By BTN_SALVAR_TELEFONE = By.xpath("//*[@id='phone_hv_editing_section']/input[2]");
	
	private String nome = "Selenium Webdriver";
	private String email = "email@test.com";
	private String telefone = "12 3456-7890";

	public void segundoDesafio() {
		editarNome();
		editarEmail();
		editarTelefone();
		validarCampos();
	}

	private void editarNome() {
		Utils.takeScreenshot(false);
		click(driver.findElement(DISPLAY_NOME));
		preencherCampo(driver.findElement(INPUT_NOME), nome);
		click(driver.findElement(BTN_SALVAR_NOME));
		aguardarElementoVisivel(driver.findElement(DISPLAY_NOME));
	}

	private void editarEmail() {
		Utils.takeScreenshot(false);
		click(driver.findElement(DISPLAY_EMAIL));
		preencherCampo(driver.findElement(INPUT_EMAIL), email);
		click(driver.findElement(BTN_SALVAR_EMAIL));
		aguardarElementoVisivel(driver.findElement(DISPLAY_EMAIL));
	}

	private void editarTelefone() {
		Utils.takeScreenshot(false);
		click(driver.findElement(DISPLAY_TELEFONE));
		preencherCampo(driver.findElement(INPUT_TELEFONE), telefone);
		click(driver.findElement(BTN_SALVAR_TELEFONE));
		aguardarElementoVisivel(driver.findElement(DISPLAY_TELEFONE));
	}

	private void validarCampos() {
		Assert.assertEquals(driver.findElement(DISPLAY_NOME).getText(), nome);
		System.out.println("Nome validado com sucesso! [" + nome + "]");
		Assert.assertEquals(driver.findElement(DISPLAY_EMAIL).getText(), "Email: " + email);
		System.out.println("E-mail validado com sucesso! [" + email + "]");
		Assert.assertEquals(driver.findElement(DISPLAY_TELEFONE).getText(), "Telefone: " + telefone);
		System.out.println("Telefone validado com sucesso! [" + telefone + "]");
		Utils.takeScreenshot(false);
	}

}