package br.unipe.page;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.unipe.common.Selenium;
import br.unipe.util.Utils;

public class BasePage {
	
	public WebDriver driver = Selenium.getDriver();
	
	private static final int LOAD_TIMEOUT = 30;

	public void aguardarElementoVisivel(WebElement element){
		try {
			WebDriverWait driverWait = new WebDriverWait(Selenium.getDriver(), LOAD_TIMEOUT);
			driverWait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			Utils.takeScreenshot(true);
			Assert.fail("Tempo excedido para aguardar elemento: "+element);
		}
	}
	
	public void click(WebElement element){
		try {
			aguardarElementoVisivel(element);
			element.click();
		} catch (Exception e) {
			Utils.takeScreenshot(true);
			Assert.fail("Nao foi possivel encontrar o elemento para clicar: "+element +". Pagina: " +Selenium.getDriver().getTitle()+"\n "+e.getMessage());
		}
	}
	
	public void preencherCampo(WebElement element, CharSequence... keysToSend) {
		try {
			aguardarElementoVisivel(element);
			element.clear();
			element.sendKeys(keysToSend);
		} catch (WebDriverException e) {
			Utils.takeScreenshot(true);
			Assert.fail("Nao foi possivel encontrar o elemento para preencher: " + element + ". Pagina: " + driver.getTitle() + "\n " + e.getMessage());
		}
	}

}