package br.unipe.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.unipe.test.PrimeiroDesafioTest;
import br.unipe.test.SegundoDesafioTest;


@RunWith(Suite.class)
@SuiteClasses
({
	PrimeiroDesafioTest.class,
	SegundoDesafioTest.class
})
public class AllTests {

}